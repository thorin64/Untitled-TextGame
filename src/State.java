public class State {
    private Player player;
    private int[][] map;
    private boolean hasKey;
    private boolean bossFight;

    State(int[][] map) {
        this.map = map;
    }

    public void setMap(int[][] map) {
        this.map = map;
    }

    public int[][] getMap() {
        return this.map;
    }

    public int mapTileAt(int X, int Y) {
        return map[Y][X];
    }

    public boolean isMapTileWalkable(int X, int Y) {
        return map[Y][X] != 0;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setHasKey(boolean hasKey) {
        this.hasKey = hasKey;
    }

    public boolean getHasKey() {
        return hasKey;
    }
    public void setBossFight(boolean bossFight) {
        this.bossFight = bossFight;
    }

    public boolean getBossFight() {
        return bossFight;
    }
}
