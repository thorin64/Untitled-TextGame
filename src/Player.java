/**
* .--------------------------------------------------------------------------------------------.
* | _   _       _   _ _   _          _           _____ _             ____                      |
* || | | |_ __ | |_(_) |_| | ___  __| |         |_   _| |__   ___   / ___| __ _ _ __ ___   ___ |
* || | | | '_ \| __| | __| |/ _ \/ _` |  _____    | | | '_ \ / _ \ | |  _ / _` | '_ ` _ \ / _ \|
* || |_| | | | | |_| | |_| |  __/ (_| | |_____|   | | | | | |  __/ | |_| | (_| | | | | | |  __/|
* | \___/|_| |_|\__|_|\__|_|\___|\__,_|           |_| |_| |_|\___|  \____|\__,_|_| |_| |_|\___||
* '--------------------------------------------------------------------------------------------'
*/
// Classe `Player` implementando a interface `Entity`.
public class Player implements Entity {
    // Declaramos os atributos seguintes como `private`.
    // Só podem ser acessadas atraves dos metodos publicos
    // declarados abaixo.
    private String name;
    private int posX;
    private int posY;

    // Declaramos os atributos seguintes como `protected`.
    // Assim classes filhas, como `Warrior`,
    // terão acesso a esses atributos tambem.
    protected int health;
    protected int stamina;
    protected int strength;

    // Metodo construtor para a class `Player`.
    Player(String name) {
  
        this.name = name;
        this.health = 20;
        this.stamina = 10;
        this.strength = 4;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int X) {
        this.posX = X;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int Y) {
        this.posY = Y;
    }

    public void walkNorth() {
        this.posY -= 1;
    }

    public void walkSouth() {
        this.posY += 1;
    }

    public void walkWest() {
        this.posX -= 1;
    }

    public void walkEast() {
        this.posX += 1;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    // Implementação da interface `Entity`,
    // com os metodos `heal`, `takeDamage` e `attack`.
    
    public void heal(int amount) {
        if (amount < 0) {
            // Lançando uma exceção ao receber argumentos não desejados.
            throw new IllegalArgumentException("Amount cannot be negative.");
        } else { this.health += amount; }
    }
    
    public void rest(int amount) {
        if (amount < 0) {
            // Lançando uma exceção ao receber argumentos não desejados.
            throw new IllegalArgumentException("Amount cannot be negative.");
        } else { this.stamina += amount; }
    }

    public void takeDamage(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        if (this.health - amount < 0) {
            this.health = 0;
        } else {
            this.health -= amount;
        }
    }

    public void attack(Entity e, int amount) {
        e.takeDamage(amount);
        this.stamina -= 1;
    }
}
