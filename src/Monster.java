/**
* .--------------------------------------------------------------------------------------------.
* | _   _       _   _ _   _          _           _____ _             ____                      |
* || | | |_ __ | |_(_) |_| | ___  __| |         |_   _| |__   ___   / ___| __ _ _ __ ___   ___ |
* || | | | '_ \| __| | __| |/ _ \/ _` |  _____    | | | '_ \ / _ \ | |  _ / _` | '_ ` _ \ / _ \|
* || |_| | | | | |_| | |_| |  __/ (_| | |_____|   | | | | | |  __/ | |_| | (_| | | | | | |  __/|
* | \___/|_| |_|\__|_|\__|_|\___|\__,_|           |_| |_| |_|\___|  \____|\__,_|_| |_| |_|\___||
* '--------------------------------------------------------------------------------------------'
*/
import java.util.Random;

// Classe `Monster` implementando a interface `Entity`.
public class Monster implements Entity {
    private String name;
    private int health;
    private int stamina;
    private int strength;

    private final String[] enemyList = {
        "Rato Infectado",
        "Peste Viva",
        "Humano Enlouquecido",
        "Cão Derretido"
    };

    Monster() {
        Random random = new Random();
        this.name = enemyList[random.nextInt(enemyList.length)];
        this.health = random.nextInt(18);
        this.strength = random.nextInt(5);
        this.stamina = random.nextInt(9);
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public int getStamina() {
        return stamina;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void heal(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        this.health += amount;
    }

    public void takeDamage(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        if (this.health - amount < 0) {
            this.health = 0;
        } else {
            this.health -= amount;
        }
    }

    public void attack(Entity e, int amount) {
        e.takeDamage(amount);
        this.stamina -= 1;
    }
}
