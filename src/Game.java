/**
* .--------------------------------------------------------------------------------------------.
* | _   _       _   _ _   _          _           _____ _             ____                      |
* || | | |_ __ | |_(_) |_| | ___  __| |         |_   _| |__   ___   / ___| __ _ _ __ ___   ___ |
* || | | | '_ \| __| | __| |/ _ \/ _` |  _____    | | | '_ \ / _ \ | |  _ / _` | '_ ` _ \ / _ \|
* || |_| | | | | |_| | |_| |  __/ (_| | |_____|   | | | | | |  __/ | |_| | (_| | | | | | |  __/|
* | \___/|_| |_|\__|_|\__|_|\___|\__,_|           |_| |_| |_|\___|  \____|\__,_|_| |_| |_|\___||
* '--------------------------------------------------------------------------------------------'
*/
/*"The ASCII Art generated from 'Text to ASCII Art' is free to use for any purpose, 
 * including personal and commercial projects. No attribution is required. However, 
 * if you find the tool useful and would like to support us, 
 * we'd appreciate a link back or a mention to help spread the word!"
 * https://www.asciiart.eu
 */


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Game {
    private State state;

    Game (State initialState) {
        this.setState(initialState);
    }

    public void run() {
        introduction();
        try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			System.out.println("An error occured: " + e.toString());
		}
        String play = getUserInput("Explorar (S) / Voltar para cela (N)\n> ");

        while (play.contains("s") != true) {
	        switch (play.toLowerCase()) {
	        case "n":
	            say("- Você volta para cela, e deita, esperando que tudo aquilo fosse um sonho...\n"
	            		+ "- Até que algo se esgueira pela abertura da cela e o devora vivo,\n"
	            		+ "- antes que você sequer pudesse perceber que monstro terrível era.\n"
	            		+ "  ____                         ___                 \n"
	            		+ " / ___| __ _ _ __ ___   ___   / _ \\__   _____ _ __ \n"
	            		+ "| |  _ / _` | '_ ` _ \\ / _ \\ | | | \\ \\ / / _ \\ '__|\n"
	            		+ "| |_| | (_| | | | | | |  __/ | |_| |\\ V /  __/ |   \n"
	            		+ " \\____|\\__,_|_| |_| |_|\\___|  \\___/  \\_/ \\___|_|   ");
	            System.exit(0);
	        default:
	        	//System.out.println("\"Explorar (S) / Voltar para cela (N)");
	        	String tryAgain = getUserInput("Explorar (S) / Voltar para cela (N)\n> ");
	        	play = tryAgain.toLowerCase();
	        }
        }

        createCharacter();

        say("Você está pronto para sua primeira missão.");

        this.getState().getPlayer().setPosX(1);
        this.getState().getPlayer().setPosY(1);
        this.getState().setHasKey(false);

        boolean running = true;
        while (running) {
            currentMessage();
            
            String response = getUserInput("> ");
            handleInput(response);
        }
    }

    private void introduction() {
        String logo = ".--------------------------------------------------------------------------------------------.\n"
        		+ "| _   _       _   _ _   _          _           _____ _             ____                      |\n"
        		+ "|| | | |_ __ | |_(_) |_| | ___  __| |         |_   _| |__   ___   / ___| __ _ _ __ ___   ___ |\n"
        		+ "|| | | | '_ \\| __| | __| |/ _ \\/ _` |  _____    | | | '_ \\ / _ \\ | |  _ / _` | '_ ` _ \\ / _ \\|\n"
        		+ "|| |_| | | | | |_| | |_| |  __/ (_| | |_____|   | | | | | |  __/ | |_| | (_| | | | | | |  __/|\n"
        		+ "| \\___/|_| |_|\\__|_|\\__|_|\\___|\\__,_|           |_| |_| |_|\\___|  \\____|\\__,_|_| |_| |_|\\___||\n"
        		+ "'--------------------------------------------------------------------------------------------'\n";
    	
    	String story1 =
        	//	String.format("""%s""", this.state.getPlayer().getName());
        		"""	
        		- Esta é a pequena cidade de Naju, vítima da facínora expansão imperialista dos Reinados Unidos,
        		- há 19 atrás, você foi detido por interromper uma execução militar, e continuava neste estado até que,
        		- um certo dia, você ouve uma explosão de um impacto, que faz as duas pessoas da unidade correrem para fora e checar a situação.
        		-
        		- De repente, você ouve gritos de desespero e sons de carne sendo dilacerada por o que parecem ser dentes afiados.
        		------------------------------------------------------------------------------------------------------------------""";
       
        String story2 =            
        	"""
        	- Devido ao impacto, e a frágil construção militar imposta pelos imperialistas, 
        	- uma grande porta de ferro cai em sua direção,
        	- arrebentando a grade de sua cela, abrindo um caminho para escapar.
        	-
        	- Você, em um ato de medo, escapa da cela e no meio da correria tropeça em uma raíz de árvore no chão,
        	- você cai, bate a cabeça e desmaia, despertando, ainda tonto, de sua queda.
        	-
        	- Aos poucos você recupera os sentidos, olha em volta e vê algo correndo pra uma direção que você não sabe dizer qual...
        	- 
        	>>> Deseja explorar ou voltar para sua cela?""";
        System.out.println(logo);   
        System.out.println(story1);
        System.out.println(story2);
    }

    private void currentMessage() {
        int X = this.getState().getPlayer().getPosX();
        int Y = this.getState().getPlayer().getPosY();

        int currentTile = this.getState().mapTileAt(X, Y);

        showPossiblePaths();

        switch (currentTile) {
        case 100:
            say("Quase não há sinais de vida, o que um dia foi sua cidade, seu bairro, sua casa, hoje é apenas cimento coberto por galhos e grama.");
            break;
        case 2:
            say("Na distância você sente uma presença estranha, talvez não apenas uma, mas várias.");
            break;
        case 4:
            say("Você percebe que há algo vivo por perto, mas não consegue ver bem o que é.");
            break;
        case 6:
            // batalha
            say("Você encontra algo que te remete a um monstro! Cuidado!!!.");
            this.getState().getMap()[Y][X] = 1;
            enterBattle();
            break;
        case 7:
            say("Você encontrou um cartão de acesso no chão.\nO que você faz?");
            handleInput(getUserInput("> "));
            break;
        case 8:
            this.getState().getMap()[Y][X] = 1;
            enterBattle();
            break;
        case 10:
            // boss
            this.getState().getMap()[Y][X] = 1;
            this.getState().setBossFight(true);
            enterBattle();
            say("Você vê o que parece ser um bunker esquecido, ele devia pertencer a um Reinunidense...");
            break;
        case 1000:
            if (this.getState().getHasKey()) {
                say(String.format("Enfim, %s, você entra no bunker, apesar de empoeirado,"
                		+ " ele contém alguns mantimentos básicos, "
                		+ "e uma grande porta com uma tranca de cofre enferrujada, "
                		+ "você tranca a porta e senta no chão, "
                		+ "finalmente você sente que está protegido, "
                		+ "mas isso é só por enquanto...", this.getState().getPlayer().getName()));
                say("··············································\n"
                		+ ": ____| _)                               __ \\:\n"
                		+ ": |      |  __ `__ \\                        /:\n"
                		+ ": __|    |  |   |   |                     _| :\n"
                		+ ":_|     _| _|  _|  _|      _) _) _)       _) :\n"
                		+ "··············································");
                System.exit(0);
            } else {
                say("Acesso negado, cartão de acesso não encontrado.");
            }
        default:
            break;
        }
    }

    private void enterBattle() {
    	Random random = new Random();
        Monster monster = new Monster();
        System.out.printf("Um %s surgiu de repente.\n", monster.getName());

        boolean fighting = true;
        while (fighting) {
            System.out.println("Você decide: 'atacar', 'curar' ou 'fugir'?");
            String action = getUserInput("> ").toLowerCase();

            switch (action) {
            case "atacar":
            	if (this.getState().getPlayer().getStamina() < 1) {
            		System.out.println("Você está muito cansado, e por isso tenta respirar e recuperar um pouco de energia!");
            		this.getState().getPlayer().rest(1);
            	} else {
                this.getState().getPlayer().attack(monster, this.getState().getPlayer().getStrength());
                System.out.printf("Você atacou o %s!\n", monster.getName());
            	}
                break;
            case "curar":
                this.getState().getPlayer().heal(random.nextInt(12));
                System.out.println("Você se curou!");
                break;
            case "fugir":
                System.out.println("Você fugiu da batalha!");
                fighting = false;
                break;
            case "ajuda":
            	helpPrint();
                break;
            default:
                System.out.println("Ação desconhecida!");
                continue;
            }

            if (monster.getHealth() > 0) {
                monster.attack(this.getState().getPlayer(), monster.getStrength());
                System.out.printf("O %s atacou você!\n", monster.getName());
            }

            System.out.printf("Seu status: %d vida, %d energia.\n",
                              this.getState().getPlayer().getHealth(),
                              this.getState().getPlayer().getStamina());
            System.out.printf("Status do %s: %d vida.\n", monster.getName(), monster.getHealth());

            if (this.getState().getPlayer().getHealth() <= 0) {
                System.out.println("Você morreu.\n  ____                            ___                    \n"
                		+ " / ___|  __ _  _ __ ___    ___   / _ \\ __   __ ___  _ __ \n"
                		+ "| |  _  / _` || '_ ` _ \\  / _ \\ | | | |\\ \\ / // _ \\| '__|\n"
                		+ "| |_| || (_| || | | | | ||  __/ | |_| | \\ V /|  __/| |   \n"
                		+ " \\____| \\__,_||_| |_| |_| \\___|  \\___/   \\_/  \\___||_|   ");
                
                System.exit(0);
            } else if (monster.getHealth() <= 0) {
                System.out.println("Você venceu a batalha!");
                fighting = false;
            }
        }
    }

    private void helpPrint() {
        String help =            
            	"""
            	
            	- Em untitled temos comandos simples! 
            	-
            	- MOVIMENTAÇÃO ⤵
            	- Digite (ir {direção}) para navegar pelo mapa
            	- As direções possíveis são: < Norte | Sul | Leste | Oeste > 
            	- O jogo sempre te dirá quais direções são possíveis de se ir, não se preocupe!
            	- 
            	- INTERAÇÃO
            	- Digite (pegar {nome do item}) para pegar algum item que aparecer!
            	- 
            	- COMBATE ⤵
            	- Estando em combate, você pode digitar:
            	- < Atacar | Curar | Fugir >
            	-  ᕙ(`▽´)ᕗ |  ☯   |ヽ(｀Д´)ﾉ
            	- 
            	- CUIDADO!!!
            	- Atacar e Curar consomem sua energia, 
            	- e para recuperá-la você deve sair de combate.
            	- Utilize com sabedoria!
            	""";
            System.out.println(help);  
    }
    
    private void handleInput(String input) {
        int X = this.getState().getPlayer().getPosX();
        int Y = this.getState().getPlayer().getPosY();

        switch (input) {
        case "sair":
            System.exit(0);
        case "ir norte":
            if (this.getState().isMapTileWalkable(X, Y - 1)) {
                this.getState().getPlayer().walkNorth();
            }
            break;
        case "ir sul":
            if (this.getState().isMapTileWalkable(X, Y + 1)) {
                this.getState().getPlayer().walkSouth();
            }
            break;
        case "ir leste":
            if (this.getState().isMapTileWalkable(X + 1, Y)) {
                this.getState().getPlayer().walkEast();
            }
            break;
        case "ir oeste":
            if (this.getState().isMapTileWalkable(X - 1, Y)) {
                this.getState().getPlayer().walkWest();
            }
            break;
        case "pegar cartão":
        case "pegar cartao":
            say("Pegou cartão.");
            this.getState().setHasKey(true);
            this.getState().getMap()[Y][X] = 1;
            break;
        case "ajuda":
        	helpPrint();
            break;
        default:
            System.out.println("Eu não sei o que isso significa. (digite (ajuda) para visualizar os comandos.)");
            break;
        }
    }

    private void createCharacter() {
        System.out.println("Escolha o nome do seu personagem:");

        String characterName = getUserInput("> ");
        System.out.println();

        switch (getCharacterClass()) {
        case 1:
            this.getState().setPlayer(new Warrior(characterName));
            break;
        case 2:
            this.getState().setPlayer(new Mage(characterName));
            break;
        case 3:
            this.getState().setPlayer(new Range(characterName));
            break;
        }
    }

    private static int getCharacterClass() {
        System.out.println("Escolha a classe do seu personagem:");
        System.out.println("  1. Guerreiro");
        System.out.println("  2. Mago");
        System.out.println("  3. Artilheiro");

        String characterClass = getUserInput("> ");
        characterClass = characterClass.toLowerCase();
        System.out.println();

        int choice = 1;

        switch (characterClass) {
        case "1":
        case "guerreiro":
            System.out.println("Parabéns, você é um Guerreiro!\n");
            choice = 1;
            break;
        case "2":
        case "mago":
            System.out.println("Parabéns, você é um Mago!\n");
            choice = 2;
            break;
        case "3":
        case "artilheiro":
            System.out.println("Parabéns, você é um Artilheiro!\n");
            choice = 3;
            break;
        default:
            return getCharacterClass();
        }

        return choice;
    }

    private void showPossiblePaths() {
        int X = this.getState().getPlayer().getPosX();
        int Y = this.getState().getPlayer().getPosY();

        int up = this.getState().mapTileAt(X, Y - 1);
        int down = this.getState().mapTileAt(X, Y + 1);
        int left = this.getState().mapTileAt(X - 1, Y);
        int right = this.getState().mapTileAt(X + 1, Y);

        int[] paths = {up, down, left, right};

        String possible = new String();
        for (int i = 0; i < paths.length; i++) {
            possible += String.format("%d ", paths[i] != 0 ? 1 : 0);
        }
        possible = possible.trim();

        switch (possible) {
            //N S W E
        case "0 0 0 0":
            say("Sem caminho adiante.");
            break;
        case "0 0 0 1":
            say("Você só pode ir para o leste.");
            break;
        case "0 0 1 0":
            say("Você só pode ir para o oeste.");
            break;
        case "0 0 1 1":
            say("Você pode ir para o oeste ou leste.");
            break;
        case "0 1 0 0":
            say("Você só pode ir para o sul.");
            break;
        case "0 1 0 1":
            say("Você pode ir para o sul ou leste.");
            break;
        case "0 1 1 0":
            say("Você pode ir para o sul ou oeste.");
            break;
        case "0 1 1 1":
            say("Você pode ir para o sul, oeste, ou leste.");
            break;
        case "1 0 0 0":
            say("Você só pode ir para o norte.");
            break;
        case "1 0 0 1":
            say("Você pode ir para o norte ou leste.");
            break;
        case "1 0 1 0":
            say("Você pode ir para o norte ou oeste.");
            break;
        case "1 0 1 1":
            say("Você pode ir para o norte, oeste, ou leste.");
            break;
        case "1 1 0 0":
            say("Você pode ir para o norte ou sul.");
            break;
        case "1 1 0 1":
            say("Você pode ir para o norte, sul, ou leste.");
            break;
        case "1 1 1 0":
            say("Você pode ir para o norte, sul, ou oeste.");
            break;
        case "1 1 1 1":
            say("Você pode ir para o norte, sul, oeste, ou leste.");
            break;
        }
    }

    private static void say(String output) {
        System.out.println(output + "\n");
    }

    private static String getUserInput(String prompt) {
        String input = new String();
        
        System.out.print(prompt);

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            input = reader.readLine();
        } catch (Exception e) {
            System.out.println("An error occured: " + e.toString());
        }

        return input;
    }

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
