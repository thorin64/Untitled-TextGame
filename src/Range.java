// Classe `Range` herdando a classe `Player`.
public class Range extends Player {
    // Metodo construtor para a class `Range`.
    Range(String name) {
        super(name);
    }

    // Implementação da interface `Entity`,
    // com os metodos `heal`, `takeDamage` e `attack`.
    // Alem de sobrescrever os metodos da classe mãe.
    @Override
    public void heal(int amount) {
        if (amount < 0) {
            // Lançando uma exceção ao receber argumentos não desejados.
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        this.health += amount + 2;
    }

    @Override
    public void takeDamage(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        if (this.health - amount < 0) {
            this.health = 0;
        } else {
            this.health -= amount + 1;
        }
    }

    @Override
    public void attack(Entity e, int amount) {
        e.takeDamage(amount);
        this.stamina -= 1;
    }
}
