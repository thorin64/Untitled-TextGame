// Classe `Warrior` herdando a classe `Player`.
public class Warrior extends Player {
    // Metodo construtor para a class `Warrior`.
    Warrior(String name) {
        super(name);
    }

    // Implementação da interface `Entity`,
    // com os metodos `heal`, `takeDamage` e `attack`.
    // Alem de sobrescrever os metodos da classe mãe.
    @Override
    public void heal(int amount) {
        if (amount < 0) {
            // Lançando uma exceção ao receber argumentos não desejados.
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        this.health += amount;
    }

    @Override
    public void takeDamage(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        if (this.health - amount < 0) {
            this.health = 0;
        } else {
            this.health -= amount;
        }
    }

    @Override
    public void attack(Entity e, int amount) {
        e.takeDamage(amount * 2);
        this.stamina -= 2;
    }
}
