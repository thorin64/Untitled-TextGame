BIN=Untitled

SRCS=src/Entity.java\
	 src/Player.java\
	 src/Warrior.java\
	 src/Mage.java\
	 src/Range.java\
	 src/Monster.java\
	 src/State.java\
	 src/Game.java\
	 src/Untitled.java\

all: $(BIN)

$(BIN):
	mkdir -p bin
	javac -d bin $(SRCS)

run: $(BIN)
	java -cp bin $(BIN)

clean:
	rm -r bin

.PHONY: all $(BIN) run clean
